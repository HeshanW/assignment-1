#include <stdio.h>
#define count 5 


//declaring the structure
struct Student
{
    char Name[20];
    char SubName[10];
    int Marks;
};

int main()
{   struct Student stu[count];
    int i,j;
    //to get the user input 
    for(i=0;i<count;i++)
    {
        printf("\nEnter the name of the student :");
        scanf("%s",stu[i].Name);
        printf("Enter the subject name :");
        scanf("%s",stu[i].SubName);
        printf("Enter the marks :");
        scanf("%d",&stu[i].Marks);
        
		printf("\n***************\n");
    }
    //to display the student details 
    printf("\n*** Student Information ***\n\n");
    for(j=0;j<count;j++)
    {
        printf("Student Name :%s\n",stu[j].Name);
        printf("Subject Name:%s\n",stu[j].SubName);
        printf("Marks :%d\n",stu[j].Marks);
        printf("\n***************\n");

    }

    return 0;  
    
}

