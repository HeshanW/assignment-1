#include <stdio.h>
#define SIZE 30

int main()
{	char chrct;  //to get the character from the user
	int i; //for the loop
	int counter=0; 
	char arr[SIZE];
	printf("ENTER A WORD :");
	fgets(arr,sizeof(arr),stdin);
	printf("ENTER A CHARACTER  : ");
	scanf("%c",&chrct);
	for(i=0;arr[i]!='\0';i++)
	{
		if(arr[i]==chrct)
			counter++;
		else 
			continue;
	}
	printf("Frequency of character '%c' is %d",chrct,counter);
	return 0;
}
