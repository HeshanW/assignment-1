#include<stdio.h>
#define ROWS 2
#define COLS 3

void read(int[ROWS][COLS]); //function to read the matrix
void print(int[ROWS][COLS]); //function to print the matrix
void add(int m1[ROWS][COLS],int m2[ROWS][COLS],int m3[ROWS][COLS]); //function to do the addition
void multi(int m1[ROWS][COLS],int m2[ROWS][COLS],int m3[ROWS][COLS]); //function to do the multiplicaction



int main(){
	int m1[ROWS][COLS]; //for matrix one
	int m2[ROWS][COLS]; //for matrix two
	int m3[ROWS][COLS]; //matrix for displaying the output
	read(m1);
	read(m2);
	print(m1);
	print(m2);
	add(m1,m2,m3);
	multi(m1,m2,m3);
	return 0;
}

void read(int m[ROWS][COLS])

{
	printf("ENTER %d * %d matrix \n",ROWS,COLS);
	int i;
	for(i=0;i<ROWS;i++)
	{
		int j;
		for(j=0;j<COLS;j++)
			scanf("%d",&m[i][j]);
	}
	
}

void print(int m[ROWS][COLS] )
{
	int i;
	printf("MATRIX");
	printf("------------------------- \n\n");
	for(i=0;i<ROWS;i++)
	{
		int j;
		for(j=0;j<COLS;j++)
			printf("\t%d\t",m[i][j]);
			
		printf("\n");
		printf("\n");
    } 
}

void add(int m1[ROWS][COLS],int m2[ROWS][COLS],int m3[ROWS][COLS])
{	int i,j;
	for(i=0;i<ROWS;i++)
		for(j=0;j<COLS;j++)
		{
			m3[i][j]=m1[i][j]+m2[i][j];
		}
	int l,m;
	printf("\nADDITION: \n");
    for (l=0;l<ROWS;l++)
        for (m=0;m<COLS;m++) 
		{
            printf("\t%d\t", m3[l][m]);
        	if(m==COLS-1)
        		printf("\n\n");
		}
		
}
void multi(int m1[ROWS][COLS],int m2[ROWS][COLS],int m3[ROWS][COLS])
{
	int i,j;
	for(i=0;i<ROWS;i++)
		for(j=0;j<COLS;j++)
		{
			m3[i][j]=m1[i][j]*m2[i][j];
		}
	int l,m;
	printf("\nMULTIPLICATION: \n");
    for (l=0;l<ROWS;l++)
        for (m=0;m<COLS;m++) 
		{
            printf("\t%d\t", m3[l][m]);
        	if(m==COLS-1)
        		printf("\n\n");
		}
}



