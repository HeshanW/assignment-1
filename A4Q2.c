/*Question 02*/
#include<stdio.h>

int main()
{
	const double PI=3.14;
	float r,h; //for radius and height
	float vol;
	printf("Enter the radius :");
	scanf("%f",&r);
	printf("Enter the height :");
	scanf("%f",&h);
	vol=PI*r*r*h/3;
	printf("Volume of the cone is %.2f",vol);
	
	return 0;
	
}
